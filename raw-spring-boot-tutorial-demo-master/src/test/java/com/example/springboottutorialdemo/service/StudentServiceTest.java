package com.example.springboottutorialdemo.service;

import com.example.springboottutorialdemo.entity.StudentEntity;
import com.example.springboottutorialdemo.exception.StudentNotFoundException;
import com.example.springboottutorialdemo.repository.StudentRepository;
import nonapi.io.github.classgraph.utils.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@SpringBootTest
class StudentServiceTest {

    @Autowired
    private StudentService studentService;
    @MockBean
    private StudentRepository studentRepository;
    @Test
    @DisplayName("Student id is present")
    public void testGetStudentById_Success() {
//        given: student_id is existing
        int existingStudentId = 1;
        StudentEntity studentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentRepository.findById(existingStudentId)).willReturn(Optional.of(studentEntity));
//        when: studentService.getStudentById is executed
        StudentEntity studentServiceResult = studentService.getStudentById(existingStudentId);
//        then: return of studentService.getStudentById should be equal to return of studentRepository.findById
        assertEquals(studentServiceResult,studentEntity);
    }

    @Test
    @DisplayName("Student id is not present")
    public void testGetStudentById_Fail() {
//        given: student_id is non-existing
        int nonExistingStudentId = 2;
        StudentEntity studentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentRepository.findById(nonExistingStudentId)).willThrow(
                new StudentNotFoundException("Student with id : "+nonExistingStudentId+" doesn't exist."));
//        when: studentService.getStudentById is executed
        StudentNotFoundException result = assertThrows(StudentNotFoundException.class,() ->{
            studentService.getStudentById(nonExistingStudentId);
        });
//        then: studentService.getStudentById will throw a StudentNotFoundException with message "Student with id : <non_existing student_id> doesn't exist."
        assertEquals("Student with id : "+nonExistingStudentId+" doesn't exist.",result.getMessage());
    }

    @Test //Test 1
    @DisplayName("Student Service Test addStudent add new StudentEntity")
    public void testAddStudent(){
        StudentEntity newStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentRepository.save(newStudentEntity)).willReturn(newStudentEntity);
        StudentEntity addedStudentEntity =  studentService.addStudent(newStudentEntity);
        assertEquals(newStudentEntity,addedStudentEntity);

    }

    @Test //Test 2
    @DisplayName("Succesful deletion of Student by id using deleteStudentById")
    public void testDeleteStudentByIdSuccess(){
        int existingStudentEntityId = 1;
        StudentEntity newStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentRepository.findById(existingStudentEntityId)).willReturn(Optional.of(newStudentEntity));
        StudentEntity studentServiceResult = studentService.deleteStudentById(existingStudentEntityId);
        assertEquals(null,studentServiceResult);
        verify(studentRepository,atLeastOnce()).delete(newStudentEntity);
    }

    @Test //Test 3
    @DisplayName("deletion of None Existing Student id using deleteStudentById")
    public void testDeleteStudentByIdFail(){
        int nonExistingStudentEntityId = 2;
        StudentEntity newStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentRepository.findById(nonExistingStudentEntityId)).willThrow(
                new StudentNotFoundException("Student with id : "+nonExistingStudentEntityId+" doesn't exist."));

        StudentNotFoundException result = assertThrows(StudentNotFoundException.class,() ->{
            studentService.getStudentById(nonExistingStudentEntityId);
        });

        assertEquals("Student with id : "+nonExistingStudentEntityId+" doesn't exist.",result.getMessage());
        verify(studentRepository,times(0)).delete(newStudentEntity);

    }

}