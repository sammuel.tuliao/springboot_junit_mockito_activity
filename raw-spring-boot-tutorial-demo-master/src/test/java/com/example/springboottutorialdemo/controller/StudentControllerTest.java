package com.example.springboottutorialdemo.controller;

import com.example.springboottutorialdemo.entity.StudentEntity;
import com.example.springboottutorialdemo.service.StudentService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@WebMvcTest(StudentController.class)
class StudentControllerTest {
    @MockBean
    private StudentService studentService;

    @Autowired
    private MockMvc mockMvc;
    @Test
    @DisplayName("PostMapping Add Student MVC")
    public void testAddStudent() throws Exception {
//        given: student Entity
        StudentEntity expectedStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentService.addStudent(expectedStudentEntity)).willReturn(expectedStudentEntity);
//        When: addStudent post request is called
        mockMvc.perform(MockMvcRequestBuilders.post("/student/")
                .contentType(MediaType.APPLICATION_JSON).content(
                                "{\r\n" +
                                        "  \"id\": 1,\r\n" +
                                        "  \"name\": \"Samm\",\r\n" +
                                        "  \"rollNo\": 1,\r\n" +
                                        "  \"address\": \"Quezon City" +"\"\r\n" +
                                        "}"
                        ))
        //Then: The request result should return an OK status
                .andExpect(MockMvcResultMatchers.status().isOk());


    }


    @Test
    @DisplayName("GetMapping List Student by ID MVC")
    public void testGetStudentById() throws Exception {
        int student_id = 1;
        StudentEntity existingStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        given(studentService.getStudentById(student_id)).willReturn(existingStudentEntity);

        mockMvc.perform(MockMvcRequestBuilders.get("/student/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(
                        "{\"id\":1,\"name\":\"Samm\",\"rollNo\":1,\"address\":\"Quezon City\"}"));
    }








}