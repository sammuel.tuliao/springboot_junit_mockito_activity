package com.example.springboottutorialdemo.repository;

import com.example.springboottutorialdemo.entity.StudentEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TestEntityManager testEntityManager;
    @Test
    @DisplayName("Retrieve Student Entity Data given student id")
    public void testFindById() {
//        given: student_id
        int studentId = 1;
        StudentEntity expectedStudentEntity = new StudentEntity(1,"Samm",1,"Quezon City");
        testEntityManager.persist(expectedStudentEntity);
//        when: studentRepository.findById is executed
        StudentEntity result = studentRepository.findById(studentId).get();
//        then: studentRepository.findById will return a studentEntity that is the same with expectedStudentEntity
        assertEquals(result,expectedStudentEntity);
    }
    @Test //Test 1
    @DisplayName("Retrieve Student Entity Data given student name")
    public void testFindByName(){
        String studentName = "Test Name";
        StudentEntity student1 = new StudentEntity(1,"Samm",1,"Quezon City");
        StudentEntity student2 = new StudentEntity(2,"Walter",2,"Cebu City");
        StudentEntity student3 = new StudentEntity(3,"Samm",3,"Malabon City");
        StudentEntity student4 = new StudentEntity(4,"Jessy",4,"Baguio City");
        StudentEntity student5 = new StudentEntity(5,"Tuco",5,"Laguna City");
        StudentEntity student6 = new StudentEntity(6,"Test Name",6,"Davao City");
        StudentEntity student7 = new StudentEntity(7,"Lalo",7,"Baliwag City");
        StudentEntity student8 = new StudentEntity(8,"Test Name",8,"San Juan City");
        StudentEntity student9 = new StudentEntity(9,"Test Name",9,"Manila City");
        testEntityManager.persist(student1);
        testEntityManager.persist(student2);
        testEntityManager.persist(student3);
        testEntityManager.persist(student4);
        testEntityManager.persist(student5);
        testEntityManager.persist(student6);
        testEntityManager.persist(student7);
        testEntityManager.persist(student8);
        testEntityManager.persist(student9);
        List<StudentEntity> result = studentRepository.findByName(studentName);

        System.out.println(result);
        assertEquals(result.get(0),student6);
        assertEquals(result.get(1),student8);
        assertEquals(result.get(2),student9);
    }


}